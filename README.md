linuxnamespaces
===============

This is a plumbing-level Python module for working with Linux namespaces via
ctypes. It leverages glibc wrappers to access the relevant system calls and
provides typed abstractions for them.

License
-------
GPL-3
